#update ile değiştirebilirsin replace = att name / değiştirmek istediğin karakter/ neyle değiştireceğin
#edit preferences sql editor > safe updates açıksa değişmez replace ile
select * from customers;

update customers set country= replace(country, '\n', '');##\n isimler arasındaki boşluğu siler datalarda , 2 kere runla
update customers set city= replace(country, '\n', '');

#view oluşturma
create view mexicanCustomers as
select customerid, customername, contactname
from customers
where country = "Mexico";

select * from mexicancustomers;##sadece mexico lı müşteriler

#orderı dahil ettik
select *
from mexicancustomers join orders on mexicancustomers.CustomerID = orders.CustomerID;

select * from products;

#çalıştıracağın sqli tara şimşeğe bas ikinci

create view productsbelowavg as
select productid, productname, price
from products
where price < (select avg(price) from products);

## orderdaki rowları siler db den > delete from orderdetails + where productId = 5;

## performans farklı ,siler deleteden hızlı > truncate orderdetails;

## parent table silinmez error verir