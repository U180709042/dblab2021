select * from shippers;

select ShipperName, count(OrderID)
from orders join shippers on orders.ShipperID=shippers.ShipperID
group by orders.ShipperID;

##en az bir sipariş veren müşteriler
select customername, orderID
from customers join orders on customers.customerID=orders.CustomerID
order by orderID;

##sipariş vermeyen müşteri isimleri de gözükür left join ile
select customername, orderID
from customers left join orders on customers.customerID=orders.CustomerID
order by orderID;

##bütün sipariş hazırlayan işçi isimleri
select firstname, lastname, orderID
from orders join employees on orders.EmployeeID=employees.EmployeeID
order by orderID;

##sipariş hazırlayan ve hazırlamayan işçiler 
select firstname, lastname, orderID
from orders right join employees on orders.EmployeeID=employees.EmployeeID
order by orderID;

##mysql de ful outer join yok union kullanarak yapılabilir.
