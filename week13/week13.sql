create temporary table products_below_avg
select productid, productname, price
from products
where Price<(select avg(Price) from products);

show table status;

select * from products_below_avg;