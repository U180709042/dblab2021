select * from proteins;
select * from proteins where pid like "5HT2C_HUMA%";
#sonuç suresi 0.047
explain select * from proteins where pid like "5HT2C_HUMA%";
#btree sonra target
#hash kullanamazsın: defult engine is InnoDB olduğu için sadece btree kullanılır.
#storage engine ı değiştirmen gerek kullanmak için, db engine type a göre index oluşur.
#hash  text ağırlıklı db ler için kullanılır
create unique index idx1 using btree on proteins(pid);
#1.  rowa yerleşti süresi de kısaldı sonuç vermenin = 0000
explain select * from proteins where pid like "5HT2C_HUMA%";
#pk lı ve pksız haline bak row sayısı değişiyor
explain select * from proteins where accession = "Q9UBA6";
#pk ekleme index olarak
alter table proteins add constraint acc_pk primary key (accession);
#indexi siliyor..
alter table proteins drop index idx1;
#pk silme
alter table proteins drop primary key;

#tablo oluştturmadan data insert etmeden bunları yazmak daha iiyimiş




